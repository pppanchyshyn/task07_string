package com.epam.task07.string.view;

import com.epam.task07.string.controller.AbstractController;
import com.epam.task07.string.controller.task2.Task2Controller;
import com.epam.task07.string.controller.task4.Task4Controller;
import com.epam.task07.string.controller.task6.Task6Controller;
import com.epam.task07.string.controller.task7.Task7Controller;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class View {
  private Scanner input = new Scanner(System.in);
  private Logger logger = LogManager.getLogger(Application.class);
  private Map<Integer, AbstractController> map;
  private static final int EXIT_MENU_ITEM = 4;

  View() {
    map = new HashMap<>();
    map.put(0, new Task2Controller());
    map.put(1, new Task4Controller());
    map.put(2, new Task6Controller());
    map.put(3, new Task7Controller());
  }

  private int enterInteger() {
    int integer;
    while (true) {
      if (input.hasNextInt()) {
        integer = input.nextInt();
        break;
      } else {
        System.out.print("Not integer! Try again: ");
        input.nextLine();
      }
    }
    return integer;
  }

  private int enterMenuItem() {
    int menuItem;
    while (true) {
      menuItem = enterInteger();
      if ((menuItem >= 0) && (menuItem <= EXIT_MENU_ITEM)) {
        break;
      } else {
        logger.info("Non-existent menu item. Try again: ");
        input.nextLine();
      }
    }
    return menuItem;
  }

  private void print(Collection<String> list) {
    for (String message : list
        ) {
      logger.info(message);
    }
  }

  private void printMenu() {
    logger.info("To test task2 press 0");
    logger.info("To test task4 press 1");
    logger.info("To test task6 press 2");
    logger.info("To test task7 press 3");
    logger.info("To quite press 4");
    logger.info("\nMake your choice: ");
  }

  public void execute() {
    int menuItem;
    while (true) {
      printMenu();
      menuItem = enterMenuItem();
      if (menuItem == EXIT_MENU_ITEM) {
        logger.info("Good luck");
        break;
      }
      print(map.get(menuItem).test());
    }
  }
}
