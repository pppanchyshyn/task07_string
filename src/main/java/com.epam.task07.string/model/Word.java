package com.epam.task07.string.model;

import java.util.List;
import java.util.stream.Collectors;

public class Word {

  private String word;
  private List<Character> characters;

  Word(String word) {
    setWord(word);
    characters = writeCharacters();
  }

  public String getWord() {
    return word;
  }


  public void setWord(String word) {
    this.word = word;
  }

  private List<Character> writeCharacters(){
    return getWord().chars().mapToObj(c->(char) c).collect(Collectors.toList());
  }

  public List<Character> getCharacters() {
    return characters;
  }

  @Override
  public String toString() {
    return getWord();
  }
}
