package com.epam.task07.string.model;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Text {

  private String filename;
  private List<String> text;
  private List<Sentence> sentences;

  public Text() {
    filename = "C:\\Users\\Pasha\\IdeaProjects\\task07_String\\src\\main\\resources\\text.txt";
    text = readText();
    sentences = writeSentences();
  }

  private List<String> readText() {
    try (Stream<String> lines = Files.lines(Paths.get(filename))) {
      text = lines.collect(Collectors.toList());
    } catch (IOException e) {
      e.printStackTrace();
    }
    return text;
  }

  private String[] parseText() {
    getText().set(0, getText().get(0).replace(".",".$"));
    getText().set(0, getText().get(0).replace("!","!$"));
    getText().set(0, getText().get(0).replace("?","?$"));
    Pattern pattern = Pattern.compile("([$])\\s*");
    return pattern.split(getText().get(0));
  }

  private List<Sentence> writeSentences(){
    List<Sentence>list = new ArrayList<>();
    Arrays.stream(parseText())
        .forEach(x->list.add(new Sentence(x)));
    return list;
  }

  public List<String> getText() {
    return text;
  }

  public List<Sentence> getSentences() {
    return sentences;
  }
}
