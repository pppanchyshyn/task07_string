package com.epam.task07.string.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

public class Sentence {

  private String sentence;
  private List<Word> words;

 Sentence(String sentence) {
    setSentence(sentence);
    words = writeWords();
  }

  private String getSentence() {
    return sentence;
  }

  private void setSentence(String sentence) {
    this.sentence = sentence;
  }

  private String[] parseSentence() {
    Pattern pattern = Pattern.compile("\\s*([\\s,.!?])\\s*");
    return pattern.split(getSentence());
  }

  private List<Word> writeWords() {
    List<Word> list = new ArrayList<>();
    Arrays.stream(parseSentence())
        .forEach(x -> list.add(new Word(x)));
    return list;
  }

  public List<Word> getWords() {
    return words;
  }

  @Override
  public String toString() {
    return getSentence();
  }
}
