package com.epam.task07.string.controller.task7;

import java.util.Comparator;

public class CompareWordsByPercentage implements Comparator<String> {

  @Override
  public int compare(String s1, String s2) {
    return (int) (((double) Task7Controller.countVowels(s1) / s1.length()
        - (double) Task7Controller.countVowels(s2) / s2.length()) * 100);
  }
}
