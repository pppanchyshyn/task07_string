package com.epam.task07.string.controller.task7;

import com.epam.task07.string.controller.AbstractController;
import java.util.Arrays;
import java.util.Collection;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Task7Controller extends AbstractController {

  public Task7Controller() {
  }

  public static int countVowels(String s) {
    Pattern vocals = Pattern.compile("(?iu)[уеіїаоєяию]");
    Matcher matcher = vocals.matcher(s);
    int vowelsCounter = 0;
    while (matcher.find()) {
      vowelsCounter++;
    }
    return vowelsCounter;
  }

  private String[] parseText() {
    Pattern pattern = Pattern.compile("\\s*([\\s,.!?;:—])\\s*");
    return pattern.split(text.getText().get(0));
  }

  @Override
  public Collection<String> test() {
    String[] strings = parseText();
    Arrays.asList(strings).sort(new CompareWordsByPercentage());
    return Arrays.asList(strings);
  }
}
