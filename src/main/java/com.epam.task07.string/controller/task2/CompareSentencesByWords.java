package com.epam.task07.string.controller.task2;

import com.epam.task07.string.model.Sentence;
import java.util.Comparator;

public class CompareSentencesByWords implements Comparator<Sentence> {

  @Override
  public int compare(Sentence sentence1, Sentence sentence2) {
    return sentence1.getWords().size() - sentence2.getWords().size();
  }
}

