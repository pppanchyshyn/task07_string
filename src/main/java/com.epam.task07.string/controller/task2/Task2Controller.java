package com.epam.task07.string.controller.task2;

import com.epam.task07.string.controller.AbstractController;
import java.util.ArrayList;
import java.util.List;

public class Task2Controller extends AbstractController {

  public Task2Controller() {
  }

  public List<String> test() {
    List<String> testResults = new ArrayList<>();
    text.getSentences()
        .stream()
        .sorted(new CompareSentencesByWords())
        .forEach(x -> testResults.add(x.toString()));
    return testResults;
  }
}
