package com.epam.task07.string.controller.task5;

import com.epam.task07.string.model.Word;
import java.util.Comparator;

public class CompareWordsByLength implements Comparator<Word>{

  @Override
  public int compare(Word o1, Word o2) {
   return o1.getCharacters().size()-o2.getCharacters().size();
  }
}
