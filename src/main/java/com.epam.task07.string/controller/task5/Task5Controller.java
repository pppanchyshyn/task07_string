package com.epam.task07.string.controller.task5;

import com.epam.task07.string.controller.AbstractController;
import com.epam.task07.string.model.Sentence;
import com.epam.task07.string.model.Word;
import java.util.Collection;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Task5Controller extends AbstractController {

  public Task5Controller() {
  }

  public String replaceFirstWordStartsWithVowel(String oldWord, String newWord) {
    Pattern pattern = Pattern.compile("^[аеєиіїоуюя].*$", Pattern.CASE_INSENSITIVE);
    Matcher matcher = pattern.matcher(oldWord);
    return matcher.replaceFirst(newWord);
  }

  public String getTheLongestWord(List<Word> words){
   Word theLongestWord = words.stream().max(new CompareWordsByLength()).get();
   return theLongestWord.getWord();
  }

  public void qwe(List<Word> words) {
    Pattern pattern = Pattern.compile("[аеєиіїоуюя].*$", Pattern.CASE_INSENSITIVE);
//    Matcher matcher = pattern.matcher("");
   words.forEach(y -> y.setWord(pattern.matcher(y.getWord()).replaceFirst(getTheLongestWord(words))));
    System.out.println(words.toString());
  }

  public void doAll(){
    text.getSentences().forEach(x -> qwe(x.getWords()));
  }

  @Override
  public Collection<String> test() {
    return null;
  }
}
