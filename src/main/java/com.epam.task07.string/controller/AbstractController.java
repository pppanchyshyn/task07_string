package com.epam.task07.string.controller;

import com.epam.task07.string.model.Text;
import java.util.Collection;

public abstract class AbstractController {

  protected Text text;

  protected AbstractController() {
    text = new Text();
  }

  public abstract Collection<String> test();
}
