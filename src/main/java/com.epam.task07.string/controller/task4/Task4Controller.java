package com.epam.task07.string.controller.task4;

import com.epam.task07.string.controller.AbstractController;
import com.epam.task07.string.model.Sentence;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class Task4Controller extends AbstractController {

  public Task4Controller() {
  }

  @Override
  public Set<String> test() {
    Set<String> testResults = new HashSet<>();
    List<Sentence> list = text
        .getSentences()
        .stream()
        .filter(x->x.toString()
            .contains("?"))
            .collect(Collectors.toList());
    list.
        forEach(x-> x.getWords()
            .stream()
            .filter(y -> y.getCharacters()
                .size() == 4)
                .forEach(y -> testResults.add(y.toString())));
    return testResults;
  }
}
