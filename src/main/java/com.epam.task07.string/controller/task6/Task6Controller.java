package com.epam.task07.string.controller.task6;

import com.epam.task07.string.controller.AbstractController;
import java.util.Arrays;
import java.util.Collection;
import java.util.regex.Pattern;

public class Task6Controller extends AbstractController {

  public Task6Controller(){
  }

  private String[] parseText() {
    Pattern pattern = Pattern.compile("\\s*([\\s,.!?;:—])\\s*");
    return pattern.split(text.getText().get(0));
  }

  @Override
  public Collection<String> test() {
    String[] strings = parseText();
    Arrays.asList(strings).sort(new CompareWordsByFirstLetter());
    return Arrays.asList(strings);
  }
}
