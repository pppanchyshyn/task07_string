package com.epam.task07.string.controller.task6;

import java.util.Comparator;

public class CompareWordsByFirstLetter implements Comparator<String> {

  @Override
  public int compare(String s1, String s2) {
    return s1.toLowerCase().charAt(0) - s2.toLowerCase().charAt(0);
  }
}
